import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/profile.dart';
import 'package:firebase_storage/firebase_storage.dart';

class FirebaseProfile {
  static final CollectionReference profiles =
      FirebaseFirestore.instance.collection('profiles');
  static FirebaseStorage firebaseStorage = FirebaseStorage.instance;

  static Future<void> addUser(
      {String fName,
      String lName,
      String email,
      String mobileNumber,
      var image}) {
    // Call the user's CollectionReference to add a new user
    return profiles
        .add({
          'name': fName + ' ' + lName, // John Doe
          'email': email, // Stokes and Sons
          'phone': mobileNumber, // 42
          'image': image
        })
        .then((value) => print("User Added $value"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  static Future<Profile> getUser({String email}) async {
    // Call the user's CollectionReference to add a new user

    return await profiles.get().then((QuerySnapshot snapshot) {
      Profile profile;
      snapshot.docs.forEach((doc) {
        if (email == doc['email']) {
          profile = Profile(
            name: doc['name'],
            email: doc['email'],
            phone: doc['phone'],
          );
        }
      });
      return profile;
    }).catchError((e) {
      print(e);
      return null;
    });
  }

  static Future<String> downloadURLExample(loginUserEmail) async {
    String downloadURL;
    downloadURL = await firebaseStorage
        .ref('uploads/$loginUserEmail.png')
        .getDownloadURL()
        .then((value) {
      return value;
    }).catchError((e) {
      print(e);
      return null;
    });
    print('download url is >>$downloadURL');
    return downloadURL;
  }
}
