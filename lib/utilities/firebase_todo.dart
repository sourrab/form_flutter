import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/todo.dart';

class FirebaseTodo {
  CollectionReference todos = FirebaseFirestore.instance.collection('todos');

  Future<List<Todo>> getUser({String email}) async {
    // Call the user's CollectionReference to add a new user
    List<Todo> todoList = [];
    return await todos.get().then((QuerySnapshot snapshot) {
      snapshot.docs.forEach((doc) {
        if (email == doc['userEmail']) {
          todoList.add(Todo(
              text: doc['text'],
              category: doc['category'],
              isChecked: doc['isChecked'],
              date: doc['date'],
              id: doc.id,
              userEmail: doc['userEmail']));
        }
      });
      return todoList;
    }).catchError((e) {
      print(e);
      return todoList;
    });
  }

  Future<DocumentReference> addTodo(
      {String text, String category, String userEmail, Timestamp date}) {
    return todos
        .add({
          'text': text,
          'category': category,
          'isChecked': false,
          'date': date,
          'userEmail': userEmail
        })
        .then((value) => value)
        .catchError((error) {
          print("Failed to add user: $error");
          return null;
        });
  }

  Future<void> updateTodo({String id, bool isChecked}) {
    return todos
        .doc(id)
        .update({'isChecked': isChecked})
        .then((value) => print("User Updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }

  Future<void> deleteTodo({String id}) {
    return todos
        .doc(id)
        .delete()
        .then((value) => print("User Deleted"))
        .catchError((error) => print("Failed to delete user: $error"));
  }
}
