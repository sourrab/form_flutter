import 'package:flutter/material.dart';

class BuildFormField extends StatelessWidget {
  final String hintText;
  final Function validator;
  BuildFormField({this.hintText, this.validator});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0),
      child: TextFormField(
        enabled: true,
        style: TextStyle(
          color: Colors.white,
        ),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(left: 30),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(50),
            borderSide: BorderSide.none,
          ),
          filled: true,
          fillColor: Color(0xff4C59A5),
          hintText: hintText,
          hintStyle: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w600,
              fontFamily: 'Secondary',
              fontSize: 20),
        ),

        validator: validator,
        // (value) {
        //   print(value);
        //   return (value != null) ? '*Required' : null;
        // },
      ),
    );
  }
}
