import 'package:flutter/material.dart';

class BuildBottomNavigationText extends StatelessWidget {
  final String text;
  final Function onPressed;
  BuildBottomNavigationText({this.text, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Do not have an account?',
          style: TextStyle(
              color: Colors.white,
              fontFamily: 'Secondary',
              fontSize: 20,
              fontWeight: FontWeight.w600),
        ),
        TextButton(
          child: Text(
            text,
            style: TextStyle(color: Color(0xffF95F3B), fontSize: 22),
          ),
          onPressed: onPressed,
        ),
      ],
    );
  }
}
