import 'package:flutter/material.dart';
import 'package:flutter_form/models/todo_data.dart';
import 'package:provider/provider.dart';
import '../models/todo.dart';
import '../constants.dart';
import '../utilities/firebase_todo.dart';

class TodoList extends StatelessWidget {
  final FirebaseTodo firebaseTodo = FirebaseTodo();
  final String show;
  TodoList({this.show});

  @override
  Widget build(BuildContext context) {
    return Consumer<TodoData>(builder: (context, todoData, child) {
      List<Todo> todoListFromModal = todoData.todoList;
      List<Todo> todoList = [];
      if (show == 'all') {
        todoList = todoListFromModal;
      } else {
        for (Todo todo in todoListFromModal) {
          if (todo.category == show) {
            todoList.add(todo);
          }
        }
      }
      print(todoList.length);

      return ListView.builder(
        itemBuilder: (context, index) {
          Color color = kCategories[todoList[index].category]['color'];

          return ListTile(
            title: Text(todoList[index].text),
            subtitle: Text(todoList[index].category),
            leading: CircleAvatar(
              backgroundColor: Colors.white,
              child: Icon(
                kCategories[todoList[index].category]['icon'],
                color: color,
              ),
            ),
            trailing: Checkbox(
              checkColor: Colors.white,
              activeColor: color,
              onChanged: (value) {
                // firebaseTodo.updateTodo(
                //     id: todoList[index].id, isChecked: value);
                todoData.updateTodo(index, value);
                print('id pressed ${todoList[index].id} $value');
              },
              value: todoList[index].isChecked,
            ),
            onLongPress: () {
              showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  backgroundColor: kColorWhite,
                  title: Text(
                    'Delete?',
                    style: TextStyle(color: Colors.red),
                  ),
                  actions: [
                    TextButton(
                      child: Text('Yes'),
                      onPressed: () {
                        Navigator.pop(context);
                        todoData.deleteTodo(index);
                      },
                    ),
                    TextButton(
                      child: Text('No'),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              );
            },
          );
        },
        itemCount: todoList.length,
      );
    });
  }
}
