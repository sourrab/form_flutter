import 'package:flutter/material.dart';

class CustomShape extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: MyCustomClipper(),
      child: Container(
        width: 70,
        height: 70,
        color: Color(0xff16A4B0),
      ),
    );
  }
}

class MyCustomClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    print(size);
    var controlPoint = Offset(0, size.height);
    var endPoint = Offset(size.width, size.height);
    Path path = Path()
      // ..moveTo(size.width / 2, 0)
      ..quadraticBezierTo(
          controlPoint.dx, controlPoint.dy, endPoint.dx, endPoint.dy)
      ..quadraticBezierTo(size.width, 0, 0, 0)
      ..close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
