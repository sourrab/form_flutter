import 'package:flutter/material.dart';

class BuildButton extends StatelessWidget {
  final Function onPressed;
  final String buttonText;
  BuildButton({this.onPressed, this.buttonText});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        primary: Color(0xffF95F3B),
        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(50),
        ),
      ),
      child: Text(
        buttonText,
        style: TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: 30,
        ),
      ),
    );
  }
}
