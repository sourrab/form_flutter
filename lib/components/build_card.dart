import 'package:flutter/material.dart';
import '../constants.dart';

class BuildCard extends StatelessWidget {
  final IconData icon;
  final String text;
  final Color color;
  final Function onTap;
  BuildCard({this.icon, this.text, this.color, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Card(
        elevation: 10,
        // color: kColorWhite.withAlpha(0xffc0),
        color: color.withAlpha(0xffd0),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Container(
          padding: const EdgeInsets.fromLTRB(8.0, 8, 4, 8),
          width: 125,
          child: Column(
            // crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: CircleAvatar(
                  child: Icon(
                    icon,
                    color: color,
                  ),
                  backgroundColor: Colors.grey[200],
                ),
              ),
              // SizedBox(
              //   height: 100,
              // ),
              Align(
                alignment: Alignment.bottomRight,
                child: Text(
                  text,
                  style: kTextStyle.copyWith(color: kColorWhite, fontSize: 23),
                  textAlign: TextAlign.end,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
