import 'package:flutter/material.dart';

class CircularContainer extends StatelessWidget {
  final double radius;
  final Color color;
  final Offset offset;
  CircularContainer({this.radius, this.color, this.offset});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Transform.translate(
        offset: offset,
        child: Container(
          height: radius,
          width: radius,
          decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(radius),
          ),
        ),
      ),
    );
  }
}
