import 'package:flutter/material.dart';
import '../constants.dart';

class BuildBottomAppBar extends StatelessWidget {
  final Function onPressedLeftButton;
  final Selected selected;
  final Function onPressedRightButton;
  BuildBottomAppBar(
      {this.selected, this.onPressedLeftButton, this.onPressedRightButton});
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      shape: CircularNotchedRectangle(),
      notchMargin: 12,
      color: kColorLightBlue,
      child: Container(
        height: 60,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: TextButton(
                style: ButtonStyle(
                  overlayColor:
                      MaterialStateProperty.all(Colors.white.withOpacity(0)),
                ),
                onPressed: onPressedLeftButton,
                child: Icon(
                  Icons.grid_view,
                  color: (selected == Selected.grid)
                      ? Colors.white
                      : Colors.white54,
                  size: (selected == Selected.grid) ? 30 : 20,
                ),
              ),
            ),
            SizedBox(
              width: 85,
            ),
            Expanded(
              child: TextButton(
                style: ButtonStyle(
                  overlayColor:
                      MaterialStateProperty.all(Colors.white.withOpacity(0)),
                ),
                onPressed: onPressedRightButton,
                child: Icon(
                  Icons.person,
                  color: (selected == Selected.profile)
                      ? Colors.white
                      : Colors.white54,
                  size: (selected == Selected.profile) ? 30 : 20,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
