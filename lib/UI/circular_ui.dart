import 'package:flutter/material.dart';
import '../components/circular_container.dart';
import '../components/custom_shape.dart';

class CircularUI extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Stack(
          children: [
            CircularContainer(
                color: Color(0xffF3C94D),
                radius: 380,
                offset: Offset(-150, -150)),
            CircularContainer(
                color: Color(0xff16A4B0),
                radius: 100,
                offset: Offset(125, 125)),
            Transform.translate(
              offset: Offset(100, -80),
              child: Container(
                color: Color(0xff111747),
                height: 150,
                width: 150,
              ),
            ),
            CircularContainer(
                color: Color(0xffF95F3B), radius: 150, offset: Offset(100, 0)),
            CircularContainer(
                color: Color(0xffF95F3B),
                radius: 150,
                offset: Offset(-50, 200)),
            CircularContainer(
                color: Color(0xff111747), radius: 75, offset: Offset(80, 240)),
          ],
        ),
        Stack(
          alignment: Alignment.topRight,
          children: [
            CircularContainer(
                color: Color(0xffFF87A2),
                radius: 175,
                offset: Offset(250, -75)),
            CircularContainer(
                color: Color(0xffF3C94D),
                radius: 115,
                offset: Offset(350, 175)),
            Transform.translate(
              offset: Offset(5, 221),
              child: Transform.rotate(
                angle: 0 * 0.0174,
                child: CustomShape(),
              ),
            ),
            CircularContainer(
                color: Color(0xffF8F9FE),
                radius: 115,
                offset: Offset(235, 175)),

            Transform.translate(
              offset: Offset(-60, 221),
              child: Transform.rotate(
                angle: 90 * 0.0174,
                child: CustomShape(),
              ),
            ),

            // CircularContainer(
            //     color: Color(0xff16A4B0),
            //     radius: 115,
            //     offset: Offset(275, 218)),

            CircularContainer(
              color: Color(0xff16A4B0),
              radius: 150,
              offset: Offset(260, 70),
            ),
          ],
        )
      ],
    );
  }
}
