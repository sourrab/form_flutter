import 'package:flutter/material.dart';
import 'package:flutter_form/models/todo_data.dart';

import 'package:flutter_form/screens/home_screen.dart';
import 'package:flutter_form/screens/profile_screen.dart';
import 'package:flutter_form/screens/signup_screen.dart';
import 'package:flutter_form/screens/login_screen.dart';
import 'package:flutter_form/screens/todo_screen.dart';
import 'constants.dart';
import 'package:provider/provider.dart';

class FlutterForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<TodoData>(
      create: (context) => TodoData(),
      child: MaterialApp(
        color: Color(0xff283793),
        theme: ThemeData.dark().copyWith(
          scaffoldBackgroundColor: Color(0xff283793),
          appBarTheme: AppBarTheme().copyWith(
            backgroundColor: kColorSecondary,
          ),
          errorColor: Colors.red[100],
        ),
        initialRoute: HomeScreen.id,
        routes: {
          HomeScreen.id: (context) => HomeScreen(),
          LoginScreen.id: (context) => LoginScreen(),
          SignUpScreen.id: (context) => SignUpScreen(),
          TodoScreen.id: (context) => TodoScreen(),
          ProfileScreen.id: (context) => ProfileScreen(),
        },
      ),
    );
  }
}
