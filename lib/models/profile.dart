class Profile {
  String name;
  String email;
  String phone;
  String imageUrl;
  Profile({this.name, this.email, this.phone});
}
