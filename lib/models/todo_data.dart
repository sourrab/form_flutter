import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'todo.dart';
import '../utilities/firebase_todo.dart';

class TodoData extends ChangeNotifier {
  List<Todo> _todoList = [];
  final FirebaseTodo firebaseTodo = FirebaseTodo();

  void getTodoListFromFirebase(List<Todo> todoList) {
    _todoList = [];
    todoList.forEach((todo) {
      _todoList.add(todo);
    });
    notifyListeners();
  }

  void addTodo({String text, String category, String loginUserEmail}) async {
    Timestamp timestamp = Timestamp.now();
    DocumentReference documentReference = await firebaseTodo.addTodo(
        text: text,
        category: category,
        userEmail: loginUserEmail,
        date: timestamp);
    _todoList.add(Todo(
        text: text,
        category: category,
        userEmail: loginUserEmail,
        date: timestamp,
        id: documentReference.id,
        isChecked: false));
    notifyListeners();
  }

  void updateTodo(index, value) {
    firebaseTodo.updateTodo(id: todoList[index].id, isChecked: value);
    _todoList[index].updateChecked();
    notifyListeners();
  }

  void deleteTodo(index) {
    firebaseTodo.deleteTodo(id: _todoList[index].id);
    _todoList.removeAt(index);
    notifyListeners();
  }

  List<Todo> get todoList {
    return _todoList;
  }

  int get length {
    return _todoList.length;
  }
}
