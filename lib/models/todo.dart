import 'package:cloud_firestore/cloud_firestore.dart';

class Todo {
  String text;
  String category;
  bool isChecked;
  Timestamp date;
  String userEmail;
  String id;

  Todo(
      {this.text,
      this.category,
      this.date,
      this.isChecked,
      this.userEmail,
      this.id});

  void updateChecked() {
    isChecked = !isChecked;
  }
}
