import 'package:flutter/cupertino.dart';
import 'package:flutter_form/utilities/firebase_profile.dart';
import 'profile.dart';

class ProfileData extends ChangeNotifier {
  Profile _profile;
  String loginUserEmail;

  ProfileData(this.loginUserEmail) {
    getProfileFromFirebase(loginUserEmail);
  }

  void getProfileFromFirebase(String loginUserEmail) async {
    _profile = await FirebaseProfile.getUser(email: loginUserEmail);
    String downloadURL =
        await FirebaseProfile.downloadURLExample(loginUserEmail);
    _profile.imageUrl = downloadURL;
    notifyListeners();
  }

  Profile get profile {
    return _profile;
  }
}
