import 'package:flutter/material.dart';

const kTextStyle = TextStyle(
  color: Colors.white70,
  fontFamily: 'Secondary',
  fontSize: 30,
  fontWeight: FontWeight.w600,
);

const Map<String, Map<String, dynamic>> kCategories = {
  'Environment': {'icon': Icons.nature_people, 'color': Colors.green},
  'Pets': {'icon': Icons.pets, 'color': Colors.blue},
  'Electricity': {'icon': Icons.electrical_services, 'color': Colors.orange},
  'Personal': {'icon': Icons.person, 'color': Colors.lightBlueAccent},
  'Others': {'icon': Icons.miscellaneous_services, 'color': Colors.yellow},
};

enum Selected { grid, profile }

const Color kColorPrimary = Color(0xff283793);
const Color kColorSecondary = Color(0xff4C59A5);
const Color kColorContrast = Color(0xffF95F3B);
const Color kColorLightBlue = Color(0xff16A4B0);
const Color kColorYellow = Color(0xffF3C94D);
const Color kColorPink = Color(0xffFF87A2);
const Color kColorWhite = Color(0xffF8F9FE);
