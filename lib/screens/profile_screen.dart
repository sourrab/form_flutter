import 'package:flutter/material.dart';
import 'package:flutter_form/constants.dart';
import 'package:provider/provider.dart';
import '../models/profile_data.dart';

class ProfileScreen extends StatelessWidget {
  static final id = 'profile_screen';

  @override
  Widget build(BuildContext context) {
    return Consumer<ProfileData>(
      builder: (context, profileData, child) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            (profileData.profile != null &&
                    profileData.profile.imageUrl != null)
                ? Image.network(
                    profileData.profile.imageUrl,
                    height: 300,
                    width: 300,
                  )
                : Image.asset('images/user.png', height: 300, width: 300),
            SizedBox(
              height: 20,
            ),
            Text(
              (profileData.profile != null) ? profileData.profile.name : '',
              style: kTextStyle,
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              (profileData.profile != null) ? profileData.profile.email : '',
              style: kTextStyle,
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              (profileData.profile != null) ? profileData.profile.phone : '',
              style: kTextStyle,
              textAlign: TextAlign.center,
            ),
          ],
        );
      },
    );
  }
}
