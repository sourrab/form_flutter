import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../constants.dart';
import '../components/build_card.dart';
import '../components/build_bottom_app_bar.dart';
import 'add_screen.dart';
import '../utilities/firebase_todo.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'profile_screen.dart';
import '../utilities/firebase_profile.dart';
// import '../components/todo_lists.dart';
import '../components/todo_list.dart';
import '../models/todo.dart';
import '../models/todo_data.dart';
import 'package:provider/provider.dart';
import '../models/profile.dart';
import '../models/profile_data.dart';

class TodoScreen extends StatefulWidget {
  static final String id = 'todo_screen';
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  Selected selected = Selected.grid;
  String show = 'all';
  String loginUserEmail;
  Map userProfile = {};
  FirebaseTodo firebaseTodo = FirebaseTodo();
  FirebaseAuth _auth = FirebaseAuth.instance;
  @override
  void initState() {
    super.initState();
    loginUserEmail = _auth.currentUser.email;
    print('login uSer is $loginUserEmail');
    // getUserProfile();
    getTodoFromFirebase(loginUserEmail);
  }

  List<BuildCard> getCardItems({Function onTap, String show}) {
    List<BuildCard> cards = [];
    kCategories.forEach((key, value) {
      cards.add(BuildCard(
        icon: value['icon'],
        text: key,
        color: value['color'],
        onTap: () {
          onTap(key);
        },
      ));
    });
    return cards;
  }

  void getTodoFromFirebase(loginUserEmail) async {
    List<Todo> todoList = await firebaseTodo.getUser(email: loginUserEmail);
    Provider.of<TodoData>(context, listen: false)
        .getTodoListFromFirebase(todoList);
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ProfileData>(
      create: (context) => ProfileData(loginUserEmail),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          centerTitle: true,
          // leading: Container(),
          title: Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Text(
              (selected == Selected.grid) ? 'Home' : 'Profile',
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          backgroundColor: kColorContrast,
          onPressed: () async {
            var res = await showDialog(
                context: context, builder: (context) => AddScreen());
            print('result from addscreen $res');
            if (res[0] == '' || res[0] == null) {
              return;
            }
            Provider.of<TodoData>(context, listen: false).addTodo(
                text: res[0], category: res[1], loginUserEmail: loginUserEmail);
          },
          tooltip: 'Increment',
          child: Icon(
            Icons.add,
          ),
          elevation: 2.0,
        ),
        bottomNavigationBar: BuildBottomAppBar(
          selected: selected,
          onPressedLeftButton: () {
            setState(() {
              selected = Selected.grid;
            });
          },
          onPressedRightButton: () {
            setState(() {
              selected = Selected.profile;
            });
          },
        ),
        body: (selected == Selected.grid)
            ? Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'QUICK CHECKLIST',
                          style: kTextStyle,
                        ),
                        Row(
                          children: [
                            Text(
                              'Show all',
                              style: kTextStyle.copyWith(fontSize: 18),
                              textAlign: TextAlign.end,
                            ),
                            Checkbox(
                                value: show == 'all' ? true : false,
                                activeColor: Colors.white54,
                                onChanged: (value) {
                                  if (value == true) {
                                    setState(() {
                                      show = 'all';
                                    });
                                  }
                                })
                          ],
                        ),
                      ],
                    ),
                    Container(
                      height: 175,
                      margin: EdgeInsets.symmetric(vertical: 8),
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: getCardItems(
                            show: show,
                            onTap: (value) {
                              setState(() {
                                show = value;
                                print('show is $show');
                              });
                            }),
                      ),
                    ),
                    Text(
                      'YOUR TASKS',
                      style: kTextStyle,
                    ),
                    Container(
                      height: 400,
                      // child: TodoList(
                      //   firebaseTodo: firebaseTodo,
                      //   loginUserEmail: loginUserEmail,
                      //   show: show,
                      // ),
                      child: TodoList(
                        show: show,
                      ),
                    )
                  ],
                ),
              )
            : ProfileScreen(),
      ),
    );
  }
}
