import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_form/constants.dart';
import '../constants.dart';

class AddScreen extends StatefulWidget {
  static final id = 'add_screen';

  @override
  _AddScreenState createState() => _AddScreenState();
}

class _AddScreenState extends State<AddScreen> {
  String dropdownValue = 'Others';
  String todoText;
  List<DropdownMenuItem> getDropdownItems() {
    List<DropdownMenuItem> dropdownItems = [];
    kCategories.forEach((key, value) {
      dropdownItems.add(DropdownMenuItem(
        value: key,
        child: Text(key),
      ));
    });
    return dropdownItems;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Container(
        constraints: BoxConstraints(maxHeight: 350),
        decoration: BoxDecoration(
          color: kColorLightBlue,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                'ADD TODO',
                style: kTextStyle.copyWith(color: Colors.white),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40.0),
                child: TextField(
                  cursorColor: kColorPrimary,
                  textAlign: TextAlign.center,
                  autofocus: true,
                  style: TextStyle(),
                  onChanged: (value) {
                    todoText = value;
                  },
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Category',
                    style: kTextStyle.copyWith(
                      fontSize: 25,
                    ),
                  ),
                  SizedBox(width: 20),
                  DropdownButton(
                    value: dropdownValue,
                    dropdownColor: kColorSecondary,
                    onChanged: (value) {
                      setState(() {
                        dropdownValue = value;
                      });
                    },
                    items: getDropdownItems(),
                  ),
                ],
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.pop(context, [
                    todoText,
                    dropdownValue,
                  ]);
                },
                child: Text(
                  'ADD',
                  style: TextStyle(color: Colors.black),
                ),
                style: ElevatedButton.styleFrom(primary: kColorYellow),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
