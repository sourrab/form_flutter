import 'package:flutter/material.dart';
import 'package:flutter_form/components/build_bottom_navigation_text.dart';
import 'package:flutter_form/screens/login_screen.dart';
import 'package:flutter_form/utilities/firebase_profile.dart';
import '../components/build_button.dart';
import '../components/build_formfield.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'todo_screen.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';

class SignUpScreen extends StatefulWidget {
  static final id = 'signup_screen';

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _formKey = GlobalKey<FormState>();
  bool showSpinner = false;

  String email;
  String password;
  String fName;
  String lName;
  String mobileNumber;
  File _image;
  final picker = ImagePicker();

  FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseStorage firebaseStorage = FirebaseStorage.instance;

  Future getImage(ImageSource source) async {
    final pickedFile = await picker.getImage(source: source);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        print(_image);
      } else {
        print('No image selected.');
      }
    });
  }

  Future<void> uploadFile(String email, File file) async {
    try {
      await firebaseStorage.ref('uploads/$email.png').putFile(file);
    } on FirebaseException catch (e) {
      // e.g, e.code == 'canceled'
      print('error while uploading to storage $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: ModalProgressHUD(
          inAsyncCall: showSpinner,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(30, 50, 30, 0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Hello...!',
                    style: TextStyle(
                      fontFamily: 'Primary',
                      color: Colors.white,
                      fontSize: 50,
                      fontWeight: FontWeight.w900,
                    ),
                  ),
                  Form(
                    key: _formKey,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 50.0),
                      child: Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                  backgroundColor: Colors.white,
                                  title: Text(
                                    'Choose image',
                                    style: TextStyle(color: Colors.black),
                                    textAlign: TextAlign.center,
                                  ),
                                  actionsPadding: EdgeInsets.all(40),
                                  actions: [
                                    ElevatedButton(
                                      child: Text('Take Photo'),
                                      onPressed: () async {
                                        getImage(ImageSource.camera);
                                        Navigator.pop(context);
                                      },
                                    ),
                                    ElevatedButton(
                                      child: Text('Gallery'),
                                      onPressed: () async {
                                        getImage(ImageSource.gallery);
                                        Navigator.pop(context);
                                      },
                                    ),
                                  ],
                                ),
                              );
                            },
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 50,
                              child: (_image == null)
                                  ? Icon(Icons.add_a_photo)
                                  : null,
                              backgroundImage:
                                  (_image != null) ? FileImage(_image) : null,
                            ),
                          ),
                          BuildFormField(
                              hintText: 'First Name',
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Name is Required';
                                }
                                fName = value;
                                return null;
                              }),
                          BuildFormField(
                            hintText: 'Last Name',
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Name is Required';
                              }
                              lName = value;
                              return null;
                            },
                          ),
                          BuildFormField(
                            hintText: 'Email',
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Email is Required';
                              }
                              final regExp =
                                  RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$');
                              if (!regExp.hasMatch(value)) {
                                return 'Email address not valid';
                              }
                              email = value;
                              return null;
                            },
                          ),
                          BuildFormField(
                            hintText: 'Password',
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Password is Required';
                              }
                              password = value;
                              return null;
                            },
                          ),
                          BuildFormField(
                            hintText: 'Mobile Number',
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Mobile Number is Required';
                              }
                              final validPhoneNoPattern =
                                  r'(^(?:[+0]9)?[0-9]{10,12}$)';
                              if (!RegExp(validPhoneNoPattern)
                                  .hasMatch(value)) {
                                return 'Enter valid phone number';
                              }
                              mobileNumber = value;
                              return null;
                            },
                          ),
                          BuildButton(
                              buttonText: 'Sign Up',
                              onPressed: () async {
                                if (_formKey.currentState.validate()) {
                                  print('email is $email $password');
                                  setState(() {
                                    showSpinner = true;
                                  });
                                  try {
                                    var res = await _auth
                                        .createUserWithEmailAndPassword(
                                            email: email, password: password);
                                    User user = res.user;
                                    await user.updateProfile(
                                        displayName: fName + ' ' + lName);

                                    await FirebaseProfile.addUser(
                                        fName: fName,
                                        lName: lName,
                                        email: email,
                                        mobileNumber: mobileNumber);
                                    uploadFile(email, _image);
                                    showTopSnackBar(
                                      context,
                                      CustomSnackBar.info(
                                        message: "User Registration Successful",
                                      ),
                                    );
                                    Navigator.popAndPushNamed(
                                      context,
                                      TodoScreen.id,
                                    );
                                  } on FirebaseAuthException catch (e) {
                                    if (e.code == 'weak-password') {
                                      showTopSnackBar(
                                        context,
                                        CustomSnackBar.error(
                                          message: "Select a stronger Password",
                                        ),
                                      );
                                    } else if (e.code ==
                                        'email-already-in-use') {
                                      showTopSnackBar(
                                        context,
                                        CustomSnackBar.error(
                                          message:
                                              "Account already exists for that email",
                                        ),
                                      );
                                    }
                                  } catch (e) {
                                    print(e);
                                  }
                                  setState(() {
                                    showSpinner = false;
                                  });
                                }
                              }),
                        ],
                      ),
                    ),
                  ),
                  BuildBottomNavigationText(
                    text: 'LOGIN',
                    onPressed: () {
                      Navigator.popAndPushNamed(context, LoginScreen.id);
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
