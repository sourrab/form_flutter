import 'package:flutter/material.dart';
import 'package:flutter_form/screens/signup_screen.dart';
import 'package:flutter_form/screens/todo_screen.dart';
import '../UI/circular_ui.dart';
import '../components/build_formfield.dart';
import '../components/build_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import '../components/build_bottom_navigation_text.dart';

class LoginScreen extends StatefulWidget {
  static final id = 'login_screen';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String email;
  String password;
  bool showSpinner = false;
  FirebaseAuth _auth = FirebaseAuth.instance;
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: ModalProgressHUD(
          inAsyncCall: showSpinner,
          opacity: 0.7,
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircularUI(),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Column(
                    children: [
                      Text(
                        'Welcome \n Back',
                        style: TextStyle(
                          fontFamily: 'Primary',
                          color: Colors.white,
                          fontSize: 50,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                      Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            BuildFormField(
                              hintText: 'Email',
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Email is Required';
                                }
                                final regExp =
                                    RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$');
                                if (!regExp.hasMatch(value)) {
                                  return 'Email address not valid';
                                }
                                email = value;
                                return null;
                              },
                            ),
                            BuildFormField(
                              hintText: 'Password',
                              validator: (value) {
                                password = (value == '') ? '123456' : value;
                                if (value == null || value.isEmpty) {
                                  return 'Password is Required';
                                }
                                password = value;
                                return null;
                              },
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            BuildButton(
                                buttonText: 'Login',
                                onPressed: () async {
                                  FocusScope.of(context).unfocus();
                                  if (_formKey.currentState.validate()) {
                                    setState(() {
                                      showSpinner = true;
                                    });
                                    try {
                                      print(email + password);
                                      UserCredential user = await _auth
                                          .signInWithEmailAndPassword(
                                              email: email, password: password);
                                      if (user != null) {
                                        showTopSnackBar(
                                          context,
                                          CustomSnackBar.success(
                                            message:
                                                "Login Successful \nWelcome ${user.user.displayName ?? ''}",
                                          ),
                                        );
                                        Navigator.popAndPushNamed(
                                            context, TodoScreen.id,
                                            arguments: user);
                                      }
                                    } on FirebaseAuthException catch (e) {
                                      print(e.code);
                                      if (e.code == 'user-not-found') {
                                        showTopSnackBar(
                                            context,
                                            CustomSnackBar.error(
                                                message: 'User not found'));
                                      }
                                    } catch (e) {
                                      showTopSnackBar(
                                        context,
                                        CustomSnackBar.error(
                                          message: "Invalid Email or Password",
                                        ),
                                      );
                                    }
                                    setState(() {
                                      showSpinner = false;
                                    });
                                  }
                                }),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                BuildBottomNavigationText(
                  text: 'SIGN-UP',
                  onPressed: () {
                    Navigator.popAndPushNamed(context, SignUpScreen.id);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
